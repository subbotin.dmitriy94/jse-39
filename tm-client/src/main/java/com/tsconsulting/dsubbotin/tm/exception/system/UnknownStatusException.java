package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class UnknownStatusException extends AbstractException {

    public UnknownStatusException() {
        super("Incorrect status entered!");
    }

}
