package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userLogin";

    @NotNull
    private final String userPassword = "userPassword";

    @NotNull
    private Session session;

    public SessionServiceTest() throws AbstractException {
        sessionService = new SessionService(new ConnectionService(new PropertyService()), new LogService());
        propertyService = new PropertyService();
        userService = new UserService(new ConnectionService(new PropertyService()), new LogService());
        userId = userService.create(userLogin, userPassword).getId();
    }

    @Before
    public void initializeTest() throws AbstractException {
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void close() throws AbstractException {
        Assert.assertEquals(userId, session.getUserId());
        Assert.assertTrue(sessionService.close(session));
        Assert.assertFalse(sessionService.close(session));
    }

    @Test
    public void validate() throws AbstractException {
        sessionService.validate(session);
    }

    @Test
    public void getUser() throws AbstractException {
        @NotNull final User tempUser = sessionService.getUser(session);
        Assert.assertEquals(userId, tempUser.getId());
        Assert.assertEquals(userLogin, tempUser.getLogin());
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final String passwordHash = HashUtil.salt(iteration, secret, userPassword);
        Assert.assertEquals(passwordHash, tempUser.getPasswordHash());
    }

    @Test
    public void getUserId() {
        Assert.assertEquals(userId, sessionService.getUserId(session));
    }

    @After
    public void finalizeTest() throws AbstractException {
        sessionService.close(session);
        userService.removeById(userId);
    }

}
