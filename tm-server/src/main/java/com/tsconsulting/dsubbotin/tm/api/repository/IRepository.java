package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void remove(@NotNull E entity) throws AbstractException, SQLException;

    void clear() throws SQLException;

    @NotNull
    List<E> findAll() throws AbstractException, SQLException;

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator) throws AbstractException, SQLException;

    @NotNull
    E findById(@NotNull String id) throws AbstractException, SQLException;

    @NotNull
    E findByIndex(int index) throws AbstractException, SQLException;

    void removeById(@NotNull String id) throws AbstractException, SQLException;

    void removeByIndex(int index) throws AbstractException, SQLException;

}