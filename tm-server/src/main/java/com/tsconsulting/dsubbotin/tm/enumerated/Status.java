package com.tsconsulting.dsubbotin.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    IN_PROGRESS("In progress", 1),
    NOT_STARTED("Not started", 2),
    COMPLETED("Completed", 3);

    @NotNull
    private final String displayName;

    private final int priority;

    Status(@NotNull String displayName, int priority) {
        this.displayName = displayName;
        this.priority = priority;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    public int getPriority() {
        return priority;
    }
}
